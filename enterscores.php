<?php
		require('templates/connectDB.php');
		require('templates/scoreSystem.php');
		require('templates/loginSystem.php');
		require('templates/header.php'); ?>

			<div id="content">

<?php
if(istEingeloggt())
{
		if(isset($_POST['submit']))
		{
			if(empty($_POST['praxis']) || empty($_POST['anzahl']))
			{
				echo('<p>Bitte Praxis wählen und Anzahl ausfüllen.</p>');
			}
			else
			{
				$erg = scoreentry($_POST['praxis'], $_POST['anzahl']);
				echo('<p>' . $erg . '</p>');
			}
		}
		else
		{
?>
				<h3>Enter Scores</h3>
				<p>Trage Praxis und Anzahl ein:</p>

				<form action="enterscores.php" method="POST">
					<div>
						<label for="verbeugungen">Verbeugungen</label>
						<input type="radio" name="praxis" id="verbeugungen" value="verbeugungen">
					</div>
					<div>
						<label for="diamantgeist">Diamantgeisst</label>
						<input type="radio" name="praxis" id="diamantgeist" value="diamantgeist">
					</div>
					<div>
						<label for="mandala">Mandala</label>
						<input type="radio" name="praxis" id="mandala" value="mandala">
					</div>
					<div>
						<label for="guruyoga">Guruyoga</label>
						<input type="radio" name="praxis" id="guruyoga" value="guruyoga">
					</div>
					<div>
						<label for="8karmapa">8. Karmapa</label>
						<input type="radio" name="praxis" id="8karmapa" value="8karmapa">
					</div>
					<br/>

					<label>Anzahl:</label>
					<input type="number" name="anzahl" />
					<br/>
					<br/>
					<input type="submit" name="submit" value="Eintragen" />
				</form>

<?php
		}
	}
	else
	{
		echo('<p>Bitte logge dich ein um mitzumachen.</p>');
		echo('<a href="login.php">Login</a>');
	}
?>

			</div>

<?php
		require("templates/footer.php");
?>
