<?php
		require('templates/connectDB.php');
		require('templates/loginSystem.php');
		require('templates/header.php'); ?>

			<div id="content">

<?php
		if(isset($_POST['submit']))
		{
			if(empty($_POST['email']) || empty($_POST['vorname']) || empty($_POST['nachname']) || empty($_POST['password']))
			{
				echo('<p>Bitte alle Felder ausfüllen.</p>');
				echo('<a href="login.php">Nochmal versuchen</a>');
			}
			else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
			{
				echo('<p>Die angegebene E-Mail Adresse ist ungültig.</p>');
				echo('<a href="login.php">Nochmal versuchen</a>');
			}
			else
			{
				$erg = register($_POST['email'], $_POST['vorname'], $_POST['nachname'], $_POST['password']);
				echo('<p>' . $erg . '</p>');
			}
		}
		else
		{
?>
				<h1>Registrieren</h1>
				<p>Hier kann man sich neu registrieren:</p>

				<form action="registrieren.php" method="POST">
					<label>E-Mail Adresse:</label>
					<input type="text" name="email" /> <br />
					<label>Vorname:</label>
					<input type="text" name="vorname" /> <br />
					<label>Nachname:</label>
					<input type="text" name="nachname" /> <br />
					<label>Passwort:</label>
					<input type="password" name="password" /> <br />
					<input type="submit" name="submit" value="Registrieren" />
				</form>
<?php
		}
?>

			</div>

<?php
		require("templates/footer.php");
?>
