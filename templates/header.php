<?php

	// Besucherzähler
	$handle = fopen("besucher.txt", "r");
	$besucherzahl = fgets($handle);
	fclose($handle);

	$besucherzahl++;

	$handle = fopen("besucher.txt", "w");
	fwrite($handle, $besucherzahl);
	fclose($handle);

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Coding</title>
		<meta charset="UTF-8" />

		<link rel="stylesheet" type="text/css" href="style/style.css" />
		<link rel="stylesheet" href="cdn/css/bootstrap.css" crossorigin="anonymous">
	</head>
	<body>
		<div id="wrapper">
			<img src="/style/Meditation.jpg" alt="Meditation">

			<?php
			if(istEingeloggt())
			{
				?>
				<nav>
					<ul>
						<li><a href="index.php">Overview</a></li>
						<li><a href="enterscores.php">Enter and Chat</a></li>
						<li><a href="login.php">Logout</a></li>
					</ul>
				</nav>
		<?php
					}
					else
					{
						?>
						<nav>
							<ul>
								<li></li>
							</ul>
						</nav>
					<?php
					}
			?>
