<?php
	session_start();

	function istEingeloggt()
	{
		if(isset($_SESSION['eingeloggt']) && $_SESSION['eingeloggt'] == 1)
		{
			return true;
		}
		return false;
	}

	function register($email, $vorname, $nachname, $password)
	{
		global $db_link;
		$email = mysqli_real_escape_string($db_link, $email);
		$name = mysqli_real_escape_string($db_link, $vorname);
		$name = mysqli_real_escape_string($db_link, $nachname);
		$password = md5($password);

		// Benutzer schon vorhanden?
		$db_res = runSQL("SELECT COUNT(*) FROM login WHERE email='" . $email . "'");
		$row = mysqli_fetch_array($db_res);

		if($row['COUNT(*)'] > 0)
		{
			echo('<a href="login.php">Nochmal versuchen</a>');
			return 'Es gibt schon einen Benutzer mit der angegebenen E-Mail Adresse';
		}

		runSQL("INSERT INTO login (email, vorname, nachname, password) VALUES ('" . $email . "', '". $vorname . "', '". $nachname . "', '" . $password . "')");
		return 'Der Benutzer wurde erfolgreich registriert.';
	}

	function login($email, $password)
	{
		global $db_link;
		$email = mysqli_real_escape_string($db_link, $email);
		$password = md5($password);

		$db_res = runSQL("SELECT * FROM login WHERE email='" . $email . "' AND password='" . $password . "'");
		if(mysqli_num_rows($db_res) == 0)
		{
			echo('<a href="login.php">Nochmal versuchen</a>');
			return 'Ungültige E-Mail oder ungültiges Passwort.';
		}

		$row = mysqli_fetch_array($db_res);
		$_SESSION['eingeloggt'] = 1;
        $_SESSION['id'] = $row['id'];
		$_SESSION['vorname'] = $row['vorname'];
        $_SESSION['verbeugungen'] = $row['verbeugungen'];
        $_SESSION['diamantgeist'] = $row['diamantgeist'];
        $_SESSION['mandala'] = $row['mandala'];
        $_SESSION['guruyoga'] = $row['guruyoga'];
        $_SESSION['8karmapa'] = $row['8karmapa'];

		header('Location: index.php');
	}

	function logout()
	{
		$_SESSION['eingeloggt'] = '';
	}
?>
