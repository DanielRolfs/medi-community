<?php
		require('templates/connectDB.php');
		require('templates/loginSystem.php');
		require('templates/header.php');
?>
	<div id="content">
<?php

		if(isset($_POST['login']))
		{
			if(empty($_POST['email']) || empty($_POST['password']))
			{
				echo("Bitte fülle E-mail und Passwort aus");
				echo('<a href="login.php">Nochmal versuchen</a>');
			}
			else
			{
				$erg = login($_POST['email'], $_POST['password']);
				echo('<p>' . $erg . '</p>');
			}
		}
		else if(isset($_POST['logout']))
		{
			logout();
			echo('<p>erfolgreich ausgeloggt.</p>');
			echo('<a href="login.php">Neu Einloggen</a>');
		}
		else if(istEingeloggt())
		{ ?>
			<p>
				<form action="login.php" method="POST">
					<input type="submit" name="logout" value="ausloggen" />
				</form>
			</p>
<?php
		}
		else
		{
?>
		<h1>Login</h1>
		<p>Willkommen zum Login. <a href="registrieren.php">neu registrieren...</a></p>

		<form action="login.php" method="POST">
			<label>E-Mail Adresse:</label>
			<input type="text" name="email" /> <br />
			<label>Passwort:</label>
			<input type="password" name="password" /> <br />
			<input type="submit" name="login" value="Login" />
		</form>

<?php
		}
?>
	</div>
<?php

		require("templates/footer.php");
?>
